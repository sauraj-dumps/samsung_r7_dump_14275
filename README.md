## r7naxx-user 12 SP1A.210812.016 N770FXXU8FVB4 release-keys
- Manufacturer: samsung
- Platform: universal9810
- Codename: r7
- Brand: samsung
- Flavor: r7naxx-user
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: N770FXXU8FVB4
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: samsung/r7naxx/r7:12/SP1A.210812.016/N770FXXU8FVB4:user/release-keys
- OTA version: 
- Branch: r7naxx-user-12-SP1A.210812.016-N770FXXU8FVB4-release-keys
- Repo: samsung_r7_dump_14275


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
